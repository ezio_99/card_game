{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TemplateHaskell  #-}
{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Redundant ^." #-}

module Control.Game
  ( runGame
  ) where

import           Control.Interaction
import           Control.Monad.Extra (ifM)
import           Data.Card
import           Data.Character
import           Data.Common
import           Data.Config
import           Data.Deck           (Deck)
import qualified Data.Deck           as Deck
import           Data.Hand           (Hand)
import qualified Data.Hand           as Hand
import qualified Data.List.NonEmpty  as NE
import           Data.Table          (Table, current, next, removeNext)
import qualified Data.Table          as Table
import qualified Data.Text.IO        as TIO
import           Fmt
import           Lens.Micro.TH       (makeLenses)
import           RIO
import           RIO.List.Partial
import           RIO.State
import           System.Random       hiding (next)
import Control.Monad (replicateM)

data PlayerState =
  PlayerState
    { _deck           :: Deck Card
    , _hand           :: Hand Card
    , _character      :: !Character
    , _mana           :: !Cost
    , _appliedEffects :: ![Effect]
    , _shield         :: !Shield
    , _health         :: !Health
    }

makeLenses ''PlayerState

-- | Contains game's state
data GameState =
  GameState
    { _playerStates :: Table PlayerState
    , _alivePlayers :: Int
    }

makeLenses ''GameState

type Game a = ReaderT Config (StateT GameState IO) a

-- | Main game logic
game :: Game ()
game = do
  isWinner <- checkWinner
  if isWinner
    then finish
    else makeTurn

checkWinner :: Game Bool
checkWinner = do
  alivePlayersCount <- gets $ view alivePlayers
  pure $ alivePlayersCount == 1

applyUsedEffectsToMe :: Game ()
applyUsedEffectsToMe = do
  effs <- gets (view $ playerStates . current . appliedEffects)
  ps <- gets (^. playerStates)
  liftIO $ mapM_ (applyEffect ps) effs
  let new = filter (\e -> e ^. duration > 0)
          . fmap (over duration (+ (-1)))
          $ effs
  modify (set (playerStates . current . appliedEffects) new)

applyEffect_ :: Effect -> Table PlayerState -> Table PlayerState
applyEffect_ e@AddShield {} t = over (current . shield) (+ (Shield $ e ^. value)) t
applyEffect_ e@DealDamage {} t = over (next . health) (+ Health dmgToHP) t1
  where
    dmg = e ^. value
    Shield currentShield = t ^. next . shield
    dmgToShield = -(min currentShield dmg)
    t1 = over (next . shield) (+ Shield dmgToShield) t
    dmgToHP = -(dmg - dmgToShield)


applyEffect :: Table PlayerState -> Effect -> IO (Table PlayerState)
applyEffect t e = do
  let nt = applyEffect_ e t
  reportAppliedEffect e t
  pure nt

reportAppliedEffect :: Effect -> Table PlayerState -> IO ()
reportAppliedEffect e@AddShield {} t = do
  let ch = t ^. current . character
  TIO.putStrLn $ "Added " +| e ^. value |+  " shields to player " +| ch ^. name |+ ""

reportAppliedEffect e@DealDamage {} t = do
  let ch = t ^. next . character
  liftIO . TIO.putStrLn $ "Dealt " +| e ^. value |+ " units of damage to " +| ch ^. name |+ ""

checkIsAlive :: Game Bool
checkIsAlive = do
  myHP <- gets (view $ playerStates . current . health)
  pure $ myHP > 0

printStatus :: PlayerState -> IO ()
printStatus ps = TIO.putStrLn s
  where
    s = "" +| ps ^. character . name |+ " has "
           +| ps ^. health |+ " HP and "
           +| ps ^. shield |+ " shield"

makeTurn :: Game ()
makeTurn = do
  c <- gets (^. playerStates . current)
  liftIO . TIO.putStrLn $ "" +| c ^. character . name |+ "'s move"
  applyUsedEffectsToMe
  liftIO . printStatus $ c
  isAlive <- checkIsAlive
  if isAlive
    then takeCardsUntilFullHand
      >> restoreMana
      >> useCards
      >> goToNextPlayer
      >> makeTurn
    else gets (^. playerStates .current . character)
     >>= liftIO . reportDeadCharacter
      >> goToNextPlayer
      >> makeTurn

takeCardsUntilFullHand :: Game ()
takeCardsUntilFullHand = do
  hand_ <- gets (^. playerStates . current . hand)
  _maxCardInHand <- asks (^. maxCardInHand)
  let cardCountToTake = _maxCardInHand - Hand.handLength hand_
  when (cardCountToTake > 0) $ do
    cards <- takeCards_ cardCountToTake
    putCardsToHand cards
  where
    takeCards_ n = do
      (cards, newDeck) <- gets (Deck.takeCards n . view (playerStates . current . deck))
      modify (set (playerStates . current . deck) newDeck)
      pure cards

    putCardsToHand cards =
      modify (over (playerStates . current . hand) (Hand.putCards cards))

restoreMana :: Game ()
restoreMana = do
  defMana <- asks (^. manaPerTurn)
  modify (set (playerStates . current . mana) defMana)

useCards :: Game ()
useCards = whenM canMove move
  where
    canMove = do
      hand_ <- gets (^. playerStates . current . hand)
      mana_ <- gets (^. playerStates . current . mana)
      pure $ mana_ >= minimum ((^. effect . cost) <$> toList hand_)

    move = do
      mana_ <- gets (^. playerStates . current . mana)
      liftIO . TIO.putStrLn $ "You have " +| mana_ |+ " points of mana"
      hand_ <- gets (^. playerStates . current . hand)
      liftIO $ printHand hand_
      askAndMove

    askAndMove = do
      hand_ <- gets (^. playerStates . current . hand)
      index <- liftIO askCardToUse
      let maybeCard = Hand.takeCard (index - 1) hand_
      case maybeCard of
        Nothing              -> askAndMove
        Just (card, newHand) -> do
          let cost_ = card ^. effect . cost
          mana_ <- gets (^. playerStates . current . mana)
          if mana_ >= cost_
            then replaceHand newHand
              >> use card
              >> removeNextPlayerIfDead
              >> ifM checkWinner finish useCards
            else liftIO (reportHaveNoManaToUse card)
              >> askAndMove

    use :: Card -> Game ()
    use card = do
      ps <- gets (^. playerStates)
      let eff = card ^. effect
      newPS <- liftIO $ applyEffect ps eff
      modify (set playerStates newPS)
      let aeff = over duration (+ (-1)) eff
      modify (over (playerStates . current . mana) (+ (-eff ^. cost)))
      when (aeff ^. duration > 0) $
        modify (over (playerStates . current . appliedEffects) (aeff :))

    replaceHand h = modify (playerStates . current . hand .~ h)

    removeNextPlayerIfDead = do
      h <- gets (^. playerStates . next . health)
      when (h <= 0) $ do
        modify (over alivePlayers (+ (-1)))
        ps <- gets (^. playerStates)
        case removeNext ps of
          Nothing       -> pure ()
          Just (_, new) -> modify (set playerStates new)

goToNextPlayer :: Game ()
goToNextPlayer = gets (^. playerStates . current)
             >>= liftIO . printStatus
              >> modify (over playerStates Table.rotatePlayers)

finish :: Game ()
finish = do
  ch <- gets (^. playerStates . current . character)
  liftIO $ reportWinner ch

genDeck :: Int -> IO (Deck Card)
genDeck n = Deck.mkDeck <$> replicateM n (random_ @Card)

random_ :: Uniform a => IO a
random_ = do
  gen <- newStdGen
  pure . fst . uniform $ gen

-- | Run the game
runGame :: Config -> IO ()
runGame config = do
  playersCount <- readPlayersCount
  names <- readCharacterNames playersCount
  defDeck <- genDeck 30
  let (hand_, deck_) = Deck.takeCards (config ^. maxCardInHand) defDeck
  let initialState = makeInitialState deck_ hand_ names
  evalStateT (runReaderT game config) initialState
  where
    makeInitialState deck_ hand_ names =
      GameState
        { _playerStates =
            Table.initPlayers $ NE.fromList $ fmap (makeSt deck_ hand_) names
        , _alivePlayers = 0
        }
    makeSt deck_ hand_ n =
      PlayerState
        { _deck = deck_
        , _hand = Hand.mkHand hand_
        , _character = Character n
        , _mana = config ^. manaPerTurn
        , _appliedEffects = []
        , _shield = 0
        , _health = 30
        }
