{-# LANGUAGE TypeApplications #-}

module Control.Interaction where

import           Data.Character (Character, name)
import           Data.Common
import           Data.Hand
import qualified Data.Text      as T
import qualified Data.Text.IO   as TIO
import           Fmt
import           RIO
import           RIO.State
import           System.IO      (getLine)
import           TextShow
import Data.Card (Card)

printHand :: (TextShow a, TextShow (Hand a)) => Hand a -> IO ()
printHand = TIO.putStrLn . showt

-- TODO: try scoped type variables
readIntWithPrefix :: Text -> IO Int
readIntWithPrefix prefix = do
    TIO.putStr prefix
    hFlush stdout
    i <- getLine
    maybe (readIntWithPrefix prefix) pure (readMaybe @Int i)

readPlayersCount :: IO Int
readPlayersCount = readIntWithPrefix "Enter number of players: "

askCardToUse :: IO Int
askCardToUse = readIntWithPrefix "Enter index of card to use: "

readCharacterNames :: Int -> IO [Name]
readCharacterNames playersCount =
    execStateT (mapM readCharacterName prefixes) []
  where
    prefixes = fmap (\x -> fmt $ "Enter player #" +| x |+ " name: ") [1 .. playersCount]

    readCharacterName p = do
        liftIO $ TIO.putStr p
        liftIO $ hFlush stdout
        n <- liftIO $ T.strip <$> TIO.getLine
        ns <- get
        if
          | T.null n         -> readCharacterName p
          | Name n `elem` ns -> reportDuplicate >> readCharacterName p
          | otherwise        -> modify (++ [Name n])

    reportDuplicate = liftIO $ TIO.putStrLn "This name already taken"

reportHaveNoManaToUse :: Card -> IO ()
reportHaveNoManaToUse card = TIO.putStrLn $ "Cannot use " +| showt card |+ " due to lack of mana"

reportWinner :: Character -> IO ()
reportWinner = TIO.putStrLn . showt

getCardIndexToUse :: IO Int
getCardIndexToUse = readIntWithPrefix "Enter index of card to use: "

reportDeadCharacter :: Character -> IO ()
reportDeadCharacter ch = TIO.putStrLn $ "Player " +| ch ^. name |+ " is dead"
