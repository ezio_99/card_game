module Data.Hand
  ( Hand
  , mkHand
  , takeCard
  , putCards
  , handLength
  , isHandEmpty
  , removeCard
  ) where

import           Fmt
import           RIO
import           RIO.List.Partial
import           RIO.Seq          ((><))
import qualified RIO.Seq          as Seq
import           TextShow

newtype Hand a =
  Hand (Seq a)
  deriving newtype (Foldable)

instance (TextShow a) => TextShow (Hand a) where
  showb hand_ = foldr1 (\i acc -> i <> "\n" <> acc)
      (zipWith (\n c -> "" +| n |+ ": " +| showb c |+ "" ) [1 ::Int ..] (toList hand_))

mkHand :: [a] -> Hand a
mkHand = Hand . Seq.fromList

takeCard :: Int -> Hand a -> Maybe (a, Hand a)
takeCard pos (Hand cards) =
  case Seq.lookup pos cards of
  Nothing -> Nothing
  Just x  -> Just (x, Hand $ Seq.deleteAt pos cards)

putCards :: [a] -> Hand a -> Hand a
putCards cards (Hand hand) = Hand $ hand >< Seq.fromList cards

handLength :: Hand a -> Int
handLength (Hand a) = Seq.length a

isHandEmpty :: Hand a -> Bool
isHandEmpty (Hand a) = Seq.null a

removeCard :: Int -> Hand a -> Hand a
removeCard pos (Hand a) = Hand $ Seq.deleteAt pos a
