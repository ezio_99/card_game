{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Redundant ^." #-}

module Data.Card where

import           Data.Common
import           Fmt
import           Lens.Micro.TH          (makeLenses)
import           RIO
import           System.Random
import           System.Random.Stateful (Uniform (uniformM), uniformRM)
import           TextShow

data Effect
  = AddShield {_cost :: Cost, _duration :: Duration, _value :: Int}
  | DealDamage {_cost :: Cost, _duration :: Duration, _value :: Int}

newtype Card = Card {_effect :: Effect}

makeLenses ''Effect
makeLenses ''Card

instance TextShow Effect where
  showb e@AddShield {} =
    "Add  " +| e ^. value |+ " points of shield "
    +| e ^. duration |+ " times (costs "
    +| e ^. cost |+ " mana)"
  showb e@DealDamage {} =
    "Deal " +| e ^. value |+ " points of damage "
    +| e ^. duration |+ " times (costs "
    +| e ^. cost |+ " mana)"

instance TextShow Card where
  showb c = "{" +| showb (c ^. effect) +| "}"

instance Uniform Effect where
  uniformM g = do
    dd <- uniformRM (1, 6) g
    let dur = Duration dd
    v <- uniformRM (1 :: Int, 6) g
    let c = Cost . ceiling $ fromIntegral dd * fromIntegral v / 3
    d <- uniformRM (0 :: Double, 1) g
    if d < 1 / 2
      then pure $ AddShield c dur v
      else pure $ DealDamage c dur v

instance Uniform Card where
  uniformM gen = do
    x <- uniformM gen
    pure $ Card x
