module Data.Table (
  Table,
  initPlayers,
  currentPlayer,
  nextPlayer,
  rotatePlayers,
  changeCurrent,
  current,
  removeCurrent,
  removeNext,
  changeNext,
  next,
) where

import           Data.CircularList  (CList)
import qualified Data.CircularList  as CL
import RIO
import qualified RIO.NonEmpty as NE

newtype Table a = Table (CList a)

initPlayers :: NonEmpty a -> Table a
initPlayers = Table . CL.fromList . NE.toList

rotatePlayers :: Table a -> Table a
rotatePlayers (Table cl) = Table $ CL.rotR cl

currentPlayer :: Table a -> a
currentPlayer (Table cl) = fromMaybe undefined (CL.focus cl)

nextPlayer :: Table a -> a
nextPlayer (Table cl) = fromMaybe undefined (CL.focus $ CL.rotR cl)

changeCurrent :: a -> Table a -> Table a
changeCurrent n (Table t) = Table $ CL.update n t

changeNext :: a -> Table a -> Table a
changeNext n t = Table . CL.rotL $ updated
  where
  (Table updated) = changeCurrent n . rotatePlayers $ t

current :: Lens' (Table a) a
current = lens currentPlayer (flip changeCurrent)

next :: Lens' (Table a) a
next = lens nextPlayer (flip changeNext)

removeCurrent :: Table a -> Maybe (a, Table a)
removeCurrent t@(Table cl) =
  if CL.isEmpty new
    then Nothing
    else Just (currentPlayer t, Table new)
  where
  new = CL.removeR cl

removeNext :: Table a -> Maybe (a, Table a)
removeNext t = do
  let t2 = rotatePlayers t
  (n, Table a) <- removeCurrent t2
  pure (n, Table $ CL.rotL a)
