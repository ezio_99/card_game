{-# LANGUAGE TemplateHaskell #-}

module Data.Config where

import           Data.Common
import           Lens.Micro.TH (makeLenses)
import           RIO
import Options.Applicative

data Config
  = Config 
  { _manaPerTurn :: Cost
  , _maxCardInHand :: Int }

makeLenses ''Config

configParser :: Parser Config
configParser =
  Config
    <$> option
      auto
      ( long "mana-per-turn"
        <> metavar "INT"
        <> showDefault
        <> value 5
        <> help "Mana per turn for every player"
      )
    <*> option
      auto
      ( long "max-card-in-hand"
        <> metavar "INT"
        <> showDefault
        <> value 6
        <> help "Max card in hand"
      )

parseConfig :: IO Config
parseConfig = execParser opts
  where
  opts =
    info
      (configParser <**> helper)
      (fullDesc <> progDesc "Simple card game")
