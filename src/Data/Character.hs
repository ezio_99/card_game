{-# LANGUAGE TemplateHaskell #-}

module Data.Character (Character (..), name) where

import           Data.Common
import           TextShow
import           Lens.Micro.TH (makeLenses)

newtype Character = Character
  { _name :: Name }

deriving newtype instance TextShow Character

makeLenses ''Character
