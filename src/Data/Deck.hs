module Data.Deck
  ( Deck
  , mkDeck
  , takeCards
  , putCards
  ) where

import RIO
import qualified RIO.Seq as Seq
import           RIO.Seq ((><))

newtype Deck a =
  Deck (Seq a)

mkDeck :: [a] -> Deck a
mkDeck = Deck . Seq.fromList

takeCards :: Int -> Deck a -> ([a], Deck a)
takeCards n (Deck cards) = (takenCards, Deck newDeck)
  where
  (a, newDeck) = Seq.splitAt n cards
  takenCards = toList a

putCards :: [a] -> Deck a -> Deck a
putCards cards (Deck deck) = Deck $ deck >< Seq.fromList cards
