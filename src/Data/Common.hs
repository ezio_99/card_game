module Data.Common where

import           Fmt      (Buildable)
import           RIO
import           TextShow (TextShow)

newtype Health =
  Health Int
  deriving newtype (Eq, Num, Ord, TextShow, Buildable)

newtype Duration =
  Duration Int
  deriving newtype (Buildable, Eq, Num, Ord, TextShow)

newtype Shield =
  Shield Int
  deriving newtype (Buildable, Eq, Num, Ord, Read, TextShow)

newtype Cost =
  Cost Int
  deriving newtype (Buildable, Eq, Num, Ord, Read, TextShow)

instance Show Cost where
  show (Cost c) = show c

newtype Name =
  Name Text
  deriving newtype (Eq, Ord, TextShow, Buildable)
