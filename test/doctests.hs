module Main where

import           Build_doctests (flags, module_sources, pkgs)
import           Data.Foldable  (traverse_)
import           RIO
import           Test.DocTest
import           Prelude (putStrLn)

main :: IO ()
main = do
    traverse_ putStrLn args
    doctest args
  where
    args = flags ++ pkgs ++ module_sources
