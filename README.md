# project-template

## Build

To build this project simply run

    make build

## Interpreter

You can run GHCi (GHC interpreter) for the whole project with

    stack ghci

or

    stack repl
