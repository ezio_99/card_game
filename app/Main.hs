module Main where

import           Control.Game (runGame)
import           Data.Config  (parseConfig)
import           RIO

main :: IO ()
main = do
    config <- parseConfig
    runGame config
